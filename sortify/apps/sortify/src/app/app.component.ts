import {AfterContentInit, AfterViewInit, Component, ElementRef, HostListener, ViewChild} from '@angular/core';
import Graph3D, {ForceGraph3DGenericInstance} from '3d-force-graph';

import * as d3 from 'd3';
import {HttpClient} from "@angular/common/http";
import {FormControl} from "@angular/forms";
import * as THREE from 'three';
import SpriteText from "three-spritetext";

interface Graph {
  nodes: {
    idx: number;
    id: string;
    node_type: string;
    label: string;
  }[];
  edges: {
    source: number;
    target: number;
  }[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {

  @ViewChild('graphContainer', {static: false})
  container: ElementRef<HTMLDivElement>

  @ViewChild('controls', {static: false})
  controls: ElementRef<HTMLDivElement>

  widthPercentage = 0.8;

  @HostListener('window:resize', ['$event'])
  onResize(event: Event) {
    const win = event.target as Window;
    this.graph
      .width(this.widthPercentage * win.innerWidth)
      .height(win.innerHeight);
  }

  svg: d3.Selection<d3.BaseType, unknown, HTMLElement, any>;
  sim: any;
  tooltip: any;

  nodes = [];
  links = [];

  node: any;
  link: any;
  zoom: any;

  playlistIDControl = new FormControl("");

  graph: ForceGraph3DGenericInstance<any>;

  constructor(private readonly http: HttpClient) {}

  ngAfterViewInit(): void {
    const rect = this.container.nativeElement.getBoundingClientRect();

    this.container.nativeElement.style.width = `${this.widthPercentage * 100}%`;
    this.controls.nativeElement.style.width = `${(1.0 - this.widthPercentage) * 100}%`;

    this.graph = Graph3D()(this.container.nativeElement)
      .enableNodeDrag(false)
      .width(this.widthPercentage * window.innerWidth)
      .height(window.innerHeight)
      .numDimensions(3)
      .nodeLabel((node: any) => node.label)
      .nodeColor((node: any) => {
        switch (node.node_type) {
          case "artist":
            return "#44b";
          case "playlist":
            return "#b44";
          case "track":
            return "#4b4";
          default:
            return "#fff";
        }
      })
      .nodeRelSize(3)
      .linkLabel((link: any) => link.edge_type)
      .linkDirectionalArrowLength(2)
      .linkDirectionalArrowRelPos(1)
      .linkWidth(1.5)
      .graphData({ nodes: this.nodes, links: this.links });

    this.http.get("http://localhost:3030/graph/").subscribe((graph: Graph) => {
      this.graph.graphData({ nodes: graph.nodes.map(node => ({ ...node, id: node.idx, weightId: node.id })), links: graph.edges });
    });
  }

  addPlaylist() {
    this.http.post("http://localhost:3030/graph/playlist", { playlist_id: this.playlistIDControl.value }).subscribe((graph: Graph) => {
      this.graph.graphData({ nodes: graph.nodes.map(node => ({ ...node, id: node.idx, weightId: node.id })), links: graph.edges });
    })
  }

  addAlbums() {
    this.http.post("http://localhost:3030/track/album", null).subscribe((graph: Graph) => {
      this.graph.graphData({ nodes: graph.nodes.map(node => ({ ...node, id: node.idx, weightId: node.id })), links: graph.edges });
    })
  }

  exploreRelatedArtists() {
    this.http.post("http://localhost:3030/graph/artist/related", null).subscribe((graph: Graph) => {
      this.graph.graphData({ nodes: graph.nodes.map(node => ({ ...node, id: node.idx, weightId: node.id })), links: graph.edges });
    })
  }

}
