module.exports = {
  name: 'sortify',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/sortify',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js',
  ],
};
