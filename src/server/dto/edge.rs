#[derive(Serialize)]
pub struct EdgeDTO {
    pub source: usize,
    pub target: usize,
    pub edge_type: String,
}
