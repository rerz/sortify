use crate::{
    graph::{edge::Edge, node::Node, Graph},
    server::dto::{edge::EdgeDTO, node::NodeDTO},
};
use actix_web::web;
use petgraph::{graph::DiGraph, Direction};
use std::sync::Arc;
use tokio::sync::RwLock;

#[derive(Serialize)]
pub struct GraphDTO {
    pub nodes: Vec<NodeDTO>,
    pub edges: Vec<EdgeDTO>,
}

pub fn create_graph_dto(graph: &DiGraph<Box<dyn Node>, Box<dyn Edge>>) -> web::Json<GraphDTO> {
    web::Json(GraphDTO {
        nodes: graph
            .node_indices()
            .map(|idx| NodeDTO {
                idx: idx.index(),
                id: graph.node_weight(idx).unwrap().get_id(),
                node_type: graph.node_weight(idx).unwrap().get_metadata().name.into(),
                label: graph.node_weight(idx).unwrap().get_label(),
            })
            .collect(),
        edges: graph
            .edge_indices()
            .map(|idx| (idx, graph.edge_endpoints(idx).unwrap()))
            .map(|(idx, (from, to))| EdgeDTO {
                source: from.index(),
                target: to.index(),
                edge_type: graph.edge_weight(idx).unwrap().get_type().into(),
            })
            .collect(),
    })
}
