#[derive(Serialize)]
pub struct NodeDTO {
    pub idx: usize,
    pub id: String,
    pub label: String,
    pub node_type: String,
}
