use crate::{
    agents::{
        artist_related_adder::RelatedArtistAdderAgent,
        playlist_track_adder::PlaylistSongAdderAgent, track_album_expander::TrackAlbumExpander,
        track_artist_expander::TrackArtistExpanderAgent, Agent, BaseAgent,
    },
    graph::Graph,
    server::{dto::graph::create_graph_dto, routes::WebError},
};
use actix_web::{web, App, Responder, Scope};
use rspotify::client::Spotify;
use std::sync::Arc;
use tokio::sync::RwLock;

pub fn get_routes() -> Scope {
    web::scope("/graph")
        .service(get_graph)
        .service(add_playlist_to_graph)
        .service(explore_related_artists)
        .service(expand_track_albums)
}

#[get("/")]
pub async fn get_graph(graph: web::Data<Arc<RwLock<Graph>>>) -> impl Responder {
    graph.read().await.to_dto()
}

#[derive(Deserialize)]
pub struct PlaylistAddRequest {
    pub playlist_id: String,
}

#[post("/playlist")]
pub async fn add_playlist_to_graph(
    (base_agent, playlist_add_request): (web::Data<BaseAgent>, web::Json<PlaylistAddRequest>),
) -> Result<impl Responder, WebError> {
    PlaylistSongAdderAgent {
        base: (**base_agent).clone(),
        playlist_id: playlist_add_request.into_inner().playlist_id,
    }
    .execute()
    .await?;

    TrackArtistExpanderAgent {
        base: (**base_agent).clone(),
    }
    .execute()
    .await?;

    Ok(base_agent.graph.read().await.to_reduced_graph())
}

#[post("/artist/related")]
pub async fn explore_related_artists(
    base_agent: web::Data<BaseAgent>,
) -> Result<impl Responder, WebError> {
    let mut related_explorer = RelatedArtistAdderAgent {
        base: (**base_agent).clone(),
    };

    related_explorer.execute().await?;

    Ok(base_agent.graph.read().await.to_reduced_graph())
}

#[post("/track/album")]
pub async fn expand_track_albums(
    base_agent: web::Data<BaseAgent>,
) -> Result<impl Responder, WebError> {
    let mut track_album_expander = TrackAlbumExpander {
        base: (**base_agent).clone(),
    };

    track_album_expander.execute().await?;

    Ok(base_agent.graph.read().await.to_dto())
}
