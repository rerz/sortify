use actix::{Actor, StreamHandler};
use actix_web::*;
use actix_web_actors::{
    ws,
    ws::{Message, ProtocolError},
};
use tokio::sync::mpsc::Receiver;

pub struct Socket {
    pub recv: Receiver<()>,
}

impl Actor for Socket {
    type Context = ws::WebsocketContext<Self>;
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for Socket {
    fn handle(&mut self, item: Result<Message, ProtocolError>, ctx: &mut Self::Context) {
        match item {
            Ok(ws::Message::Text(text)) => {}
            _ => {}
        }
    }
}
