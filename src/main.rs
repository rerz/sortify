use crate::{
    agents::{
        artist_related_adder::RelatedArtistAdderAgent,
        playlist_track_adder::PlaylistSongAdderAgent, track_album_expander::TrackAlbumExpander,
        track_artist_expander::TrackArtistExpanderAgent,
        unrelated_artist_remover::UnrelatedArtistRemover, Agent, BaseAgent,
    },
    graph::{edge::Edge, node::Node, Graph},
    socket::WebsocketMessage,
};
use actix_cors::Cors;
use actix_web::*;
use anyhow::Error;
use async_trait::async_trait;
use derive_more::Display;
use dotenv;
use futures::{FutureExt, TryFutureExt};
use listenfd::ListenFd;
use petgraph::{csr::NodeIndex, dot::Dot, graph::DiGraph, visit::NodeRef};
use rspotify::{client::Spotify, oauth2::SpotifyClientCredentials};
use serde::{Deserialize, Serialize};
use std::{
    fmt::{Formatter, Write},
    io::BufWriter,
    sync::Arc,
};
use tokio::sync::RwLock;

#[macro_use]
extern crate log;

#[macro_use]
extern crate anyhow;

#[macro_use]
extern crate serde;

#[macro_use]
extern crate actix_web;

mod agents;
mod graph;
mod server;
mod socket;
mod spotify;

pub type SendableGraph = Arc<RwLock<DiGraph<Box<dyn Node>, Box<dyn Edge>>>>;

#[actix_web::main]
async fn main() -> Result<(), anyhow::Error> {
    let mut listenfd = ListenFd::from_env();

    dotenv::dotenv().ok();
    env_logger::init();

    let cc = SpotifyClientCredentials {
        client_id: std::env::var("SPOTIFY_ID").unwrap().into(),
        client_secret: std::env::var("SPOTIFY_SECRET").unwrap().into(),
        token_info: None,
    };

    let spotify = Arc::new(Spotify::default().client_credentials_manager(cc).build());
    let graph = Arc::new(RwLock::new(Graph::new()));

    let mut server = HttpServer::new(move || {
        let base_agent = BaseAgent {
            spotify: Arc::clone(&spotify),
            graph: Arc::clone(&graph),
        };

        App::new()
            .wrap(Cors::new().send_wildcard().finish())
            .data(Arc::clone(&graph))
            .data(Arc::clone(&spotify))
            .data(base_agent)
            .service(server::routes::graph::get_routes())
            .service(socket::websocket_connection_endpoint)
    });

    server = if let Some(l) = listenfd.take_tcp_listener(0).unwrap() {
        server.listen(l)?
    } else {
        server.bind("127.0.0.1:3030")?
    };

    server.run().await;

    Ok(())
}
