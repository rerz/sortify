use crate::graph::Graph;
use actix_web::*;
use async_trait::async_trait;
use rspotify::client::Spotify;
use std::sync::Arc;
use tokio::sync::RwLock;

pub mod artist_related_adder;
pub mod playlist_track_adder;
pub mod track_album_expander;
pub mod track_artist_expander;
pub mod unrelated_artist_remover;

#[async_trait]
pub trait Agent {
    async fn execute(&mut self) -> Result<(), anyhow::Error>;
}

#[derive(Clone)]
pub struct BaseAgent {
    pub graph: Arc<RwLock<Graph>>,
    pub spotify: Arc<Spotify>,
}

#[async_trait]
impl Agent for BaseAgent {
    async fn execute(&mut self) -> Result<(), anyhow::Error> {
        unimplemented!()
    }
}
