use crate::{
    agents::{Agent, BaseAgent},
    graph::{
        edge::on_album::OnAlbumEdge,
        node::{album::AlbumNode, track::TrackNode, BaseNode, Node},
    },
};
use async_trait::async_trait;

pub struct TrackAlbumExpander {
    pub base: BaseAgent,
}

#[async_trait]
impl Agent for TrackAlbumExpander {
    async fn execute(&mut self) -> Result<(), anyhow::Error> {
        let mut graph = self.base.graph.write().await;

        info!("Adding album nodes for all track nodes");

        let track_indices = graph.get_nodes_of_type::<TrackNode>()?;

        for track_idx in track_indices {
            let node_weight = graph.get_node_mut::<TrackNode>(track_idx)?;

            if node_weight.has_been_visited_by("track_album_expander".into()) {
                continue;
            }

            node_weight.add_visited_by("track_album_expander".into());

            let album = &node_weight.spotify_track.album;
            let album = self
                .base
                .spotify
                .album(&album.id.clone().unwrap())
                .await
                .unwrap();

            let album_idx = graph.add_node(Box::new(AlbumNode {
                base: BaseNode::default(),
                spotify_album: album,
            }));

            graph
                .inner
                .add_edge(track_idx, album_idx, Box::new(OnAlbumEdge));
        }

        Ok(())
    }
}
