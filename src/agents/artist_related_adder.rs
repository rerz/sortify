use crate::{
    agents::{Agent, BaseAgent},
    graph::{
        edge::related_to::RelatedToEdge,
        node::{artist::ArtistNode, BaseNode, Node},
    },
};
use async_trait::async_trait;

pub struct RelatedArtistAdderAgent {
    pub base: BaseAgent,
}

#[async_trait]
impl Agent for RelatedArtistAdderAgent {
    async fn execute(&mut self) -> Result<(), anyhow::Error> {
        let mut graph = self.base.graph.write().await;

        let artist_indices = graph.get_nodes_of_type::<ArtistNode>()?;

        info!("Adding all related artists");

        for artist_idx in artist_indices {
            let related = {
                let node_weight = graph.get_node_mut::<ArtistNode>(artist_idx)?;

                if node_weight.has_been_visited_by("related_artist_adder".into()) {
                    None
                } else {
                    node_weight.add_visited_by("related_artist_adder".into());

                    let related = self
                        .base
                        .spotify
                        .artist_related_artists(&node_weight.spotify_artist.id)
                        .await
                        .unwrap();

                    Some(related)
                }
            };

            let related = match related {
                Some(artists) => artists,
                None => continue,
            };

            for related_artist in related.artists {
                let related_artist_idx = graph.add_node(Box::new(ArtistNode {
                    base: BaseNode::default(),
                    spotify_artist: related_artist,
                }));

                graph
                    .inner
                    .add_edge(artist_idx, related_artist_idx, Box::new(RelatedToEdge));
            }
        }

        Ok(())
    }
}
