use crate::{
    agents::{Agent, BaseAgent},
    graph::{
        edge::on_playlist::OnPlaylistEdge,
        node::{playlist::PlaylistNode, track::TrackNode, BaseNode},
    },
};
use async_trait::async_trait;
use petgraph::graph::NodeIndex;

pub struct PlaylistSongAdderAgent {
    pub base: BaseAgent,
    pub playlist_id: String,
}

#[async_trait]
impl Agent for PlaylistSongAdderAgent {
    async fn execute(&mut self) -> Result<(), anyhow::Error> {
        let mut graph = self.base.graph.write().await;

        let playlist = self
            .base
            .spotify
            .playlist(&self.playlist_id, None, None)
            .await
            .map_err(|_| anyhow!("fetch playlist"))?;

        info!(
            "Adding {} track nodes from playlist {}",
            playlist.tracks.total, &playlist.name
        );

        let playlist_idx = graph.add_node(Box::new(PlaylistNode {
            base: BaseNode::default(),
            spotify_playlist: playlist.clone(),
        }));

        let mut track_indices: Vec<NodeIndex> = vec![];

        for playlist_track in playlist.tracks.items {
            let track_idx = graph.add_node(Box::new(TrackNode {
                base: BaseNode::default(),
                spotify_track: playlist_track.track.unwrap(),
            }));

            graph
                .inner
                .add_edge(track_idx, playlist_idx, Box::new(OnPlaylistEdge));
        }

        Ok(())
    }
}
