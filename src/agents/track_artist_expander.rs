use crate::{
    agents::{Agent, BaseAgent},
    graph::{
        edge::on_track::OnTrackEdge,
        node::{artist::ArtistNode, track::TrackNode, BaseNode, Node},
    },
};
use async_trait::async_trait;
use itertools::Itertools;
use rspotify::model::artist::SimplifiedArtist;
use std::collections::HashMap;

pub struct TrackArtistExpanderAgent {
    pub base: BaseAgent,
}

#[async_trait]
impl Agent for TrackArtistExpanderAgent {
    async fn execute(&mut self) -> Result<(), anyhow::Error> {
        let mut graph = self.base.graph.write().await;

        info!("Adding artist nodes for all track nodes");

        let track_indices = graph.get_nodes_of_type::<TrackNode>()?;

        let mut track_artist_map = HashMap::<String, Vec<String>>::new();

        for track_idx in track_indices {
            let node_weight = graph.get_node_mut::<TrackNode>(track_idx)?;

            node_weight.add_visited_by("track_artist_expander".into());

            track_artist_map.insert(
                node_weight.get_id(),
                node_weight
                    .spotify_track
                    .artists
                    .iter()
                    .filter_map(|artist| artist.id.clone())
                    .collect(),
            );
        }

        let unique_artist_ids = track_artist_map
            .values()
            .flatten()
            .unique()
            .cloned()
            .collect::<Vec<_>>();

        let artists =
            crate::spotify::get_all_artists(self.base.spotify.clone(), unique_artist_ids).await;

        for artist in artists {
            graph.add_node(Box::new(ArtistNode {
                base: BaseNode::default(),
                spotify_artist: artist,
            }));
        }

        for (track_id, artists) in track_artist_map {
            let track_idx = graph.get_node_by_id::<TrackNode>(&track_id)?.unwrap();

            for artist_id in artists {
                if let Some(artist_idx) = graph.get_node_by_id::<ArtistNode>(&artist_id)? {
                    graph
                        .inner
                        .add_edge(artist_idx, track_idx, Box::new(OnTrackEdge));
                } else {
                    continue;
                }
            }
        }

        Ok(())
    }
}
