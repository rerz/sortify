use crate::{
    agents::{Agent, BaseAgent},
    graph::node::artist::ArtistNode,
};
use async_trait::async_trait;
use petgraph::graph::EdgeIndex;

pub struct UnrelatedArtistRemover {
    pub base: BaseAgent,
}

#[async_trait]
impl Agent for UnrelatedArtistRemover {
    async fn execute(&mut self) -> Result<(), anyhow::Error> {
        let mut graph = self.base.graph.write().await;

        let artist_indices = graph.get_nodes_of_type::<ArtistNode>()?;

        info!("Removing");

        for artist_idx in artist_indices {
            let edges = graph.inner.edges(artist_idx).collect::<Vec<_>>();

            if edges.len() == 1 {
                if let Some(edge) = edges.get(0) {
                    if edge.weight().get_type() == "related" {
                        info!("{:?}", edge);
                    }
                }
            }
        }

        Ok(())
    }
}
