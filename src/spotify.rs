use rspotify::client::Spotify;

use actix_web::web;
use futures::{future::join_all, StreamExt, TryStreamExt};
use itertools::Itertools;
use rspotify::model::artist::{FullArtist, FullArtists};
use std::sync::Arc;

// TODO: Make this less shitty
pub async fn get_all_artists(spotify: Arc<Spotify>, artist_ids: Vec<String>) -> Vec<FullArtist> {
    let artist_chunks = artist_ids
        .chunks(50)
        .map(|chunk| chunk.to_vec())
        .collect::<Vec<_>>();

    let mut artists: Vec<FullArtists> = vec![];

    for chunk in artist_chunks {
        artists.push(spotify.artists(chunk).await.unwrap());
    }

    artists
        .into_iter()
        .flat_map(|artists| artists.artists)
        .collect::<Vec<_>>()
}
