use crate::{
    graph::{
        edge::Edge,
        node::{Node, NodeMetadata, NodeRegistryValue},
    },
    server::dto::graph::{create_graph_dto, GraphDTO},
};
use actix_web::web;
use derive_more::Constructor;
use downcast_rs::__std::collections::HashSet;
use petgraph::{
    graph::{DiGraph, NodeIndex},
    Direction, Undirected,
};
use std::{collections::HashMap, sync::Arc};
use tokio::sync::RwLock;
use typemap_rev::{TypeMap, TypeMapKey};

pub mod edge;
pub mod node;

pub trait TypeKey: TypeMapKey {
    const KEY: &'static str;
}

pub struct NodeIndices(Vec<NodeIndex>);

pub struct Graph {
    pub registry: typemap_rev::TypeMap,
    pub inner: DiGraph<Box<dyn Node>, Box<dyn Edge>>,
}

impl Graph {
    pub fn new() -> Self {
        Self {
            registry: TypeMap::new(),
            inner: DiGraph::new(),
        }
    }

    pub fn add_node<T>(&mut self, node: Box<T>) -> NodeIndex
    where
        T: Node,
    {
        let node_id = node.get_id();

        if let Ok(Some(idx)) = self.get_node_by_id::<T>(&node_id) {
            return idx;
        }

        let metadata = node.get_metadata();

        let node_idx = self.inner.add_node(node);

        let registry_value: &mut NodeRegistryValue =
            self.registry.entry::<T>().or_insert(NodeRegistryValue {
                id_to_idx: HashMap::new(),
                name: metadata.name.into(),
            });

        registry_value.id_to_idx.insert(node_id, node_idx);

        node_idx
    }

    pub fn contains_node<T: Node>(&self, node_id: &str) -> bool {
        matches!(self.get_node_by_id::<T>(node_id), Ok(Some(_)))
    }

    pub fn get_node_by_id<T: Node>(
        &self,
        node_id: &str,
    ) -> Result<Option<NodeIndex>, anyhow::Error> {
        Ok(self
            .registry
            .get::<T>()
            .ok_or(anyhow!("unknown node type"))?
            .id_to_idx
            .get(node_id)
            .cloned())
    }

    pub fn get_nodes_of_type<T: Node>(&mut self) -> Result<Vec<NodeIndex>, anyhow::Error> {
        self.registry
            .get::<T>()
            .map(|registry_value: &NodeRegistryValue| registry_value.get_node_indices())
            .ok_or(anyhow!("unknown node type"))
    }

    pub fn get_node_mut<N: Node>(&mut self, idx: NodeIndex) -> Result<&mut N, anyhow::Error> {
        self.inner
            .node_weight_mut(idx)
            .ok_or(anyhow!("no such node"))?
            .downcast_mut::<N>()
            .ok_or(anyhow!("wrong node type"))
    }

    pub fn get_node<N: Node>(&self, idx: NodeIndex) -> Result<&N, anyhow::Error> {
        self.inner
            .node_weight(idx)
            .ok_or(anyhow!("no such node"))?
            .downcast_ref::<N>()
            .ok_or(anyhow!("wrong node type"))
    }

    pub fn to_dto(&self) -> web::Json<GraphDTO> {
        create_graph_dto(&self.inner)
    }

    pub fn to_reduced_graph(&self) -> web::Json<GraphDTO> {
        let mut graph = self.inner.clone();

        graph.retain_nodes(|graph, node| {
            let inc = graph.edges_directed(node, Direction::Incoming);
            let out = graph.edges_directed(node, Direction::Outgoing);

            inc.count() + out.count() > 1
        });

        create_graph_dto(&graph)
    }
}
