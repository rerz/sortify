use crate::graph::TypeKey;
use core::fmt::Formatter;
use downcast_rs::{impl_downcast, DowncastSync};
use petgraph::graph::NodeIndex;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use typemap_rev::TypeMapKey;

pub mod album;
pub mod artist;
pub mod playlist;
pub mod track;

pub struct NodeMetadata {
    pub name: &'static str,
}

#[typetag::serde(tag = "type")]
pub trait Node: Send + Sync + DowncastSync + TypeMapKey<Value = NodeRegistryValue> {
    fn box_clone(&self) -> Box<dyn Node>;

    fn get_id(&self) -> String;

    fn get_label(&self) -> String;

    fn get_metadata(&self) -> NodeMetadata;

    fn add_visited_by(&mut self, visitor_id: String);

    fn has_been_visited_by(&self, visitor_id: String) -> bool;
}

pub struct NodeRegistryValue {
    pub id_to_idx: HashMap<String, NodeIndex>,
    pub name: String,
}

impl NodeRegistryValue {
    pub fn get_node_indices(&self) -> Vec<NodeIndex> {
        self.id_to_idx.values().copied().collect()
    }
}

impl_downcast!(sync Node);

impl core::fmt::Debug for dyn Node {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        write!(f, "Node")
    }
}

impl core::fmt::Display for dyn Node {
    fn fmt(&self, f: &mut Formatter<'_>) -> core::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Clone for Box<dyn Node> {
    fn clone(&self) -> Self {
        self.box_clone()
    }
}

#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct BaseNode {
    pub visitation_map: HashSet<String>,
}
