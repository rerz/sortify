use crate::graph::{
    node::{BaseNode, Node, NodeMetadata, NodeRegistryValue},
    TypeKey,
};
use rspotify::model::album::FullAlbum;
use typemap_rev::TypeMapKey;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AlbumNode {
    pub base: BaseNode,
    pub spotify_album: FullAlbum,
}

#[typetag::serde]
impl Node for AlbumNode {
    fn box_clone(&self) -> Box<dyn Node> {
        Box::new(self.clone())
    }

    fn get_id(&self) -> String {
        self.spotify_album.id.clone()
    }

    fn get_label(&self) -> String {
        self.spotify_album.name.clone()
    }

    fn get_metadata(&self) -> NodeMetadata {
        NodeMetadata { name: "album" }
    }

    fn add_visited_by(&mut self, visitor_id: String) {
        self.base.visitation_map.insert(visitor_id);
    }

    fn has_been_visited_by(&self, visitor_id: String) -> bool {
        self.base.visitation_map.contains(&visitor_id)
    }
}

impl TypeMapKey for AlbumNode {
    type Value = NodeRegistryValue;
}
