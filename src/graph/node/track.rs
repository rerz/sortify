use crate::graph::{
    node::{BaseNode, Node, NodeMetadata, NodeRegistryValue},
    TypeKey,
};
use rspotify::model::track::FullTrack;
use typemap_rev::TypeMapKey;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TrackNode {
    pub base: BaseNode,
    pub spotify_track: FullTrack,
}

#[typetag::serde]
impl Node for TrackNode {
    fn box_clone(&self) -> Box<dyn Node> {
        Box::new(self.clone())
    }

    fn get_id(&self) -> String {
        self.spotify_track.id.clone().unwrap_or("".into())
    }

    fn get_label(&self) -> String {
        self.spotify_track.name.clone()
    }

    fn get_metadata(&self) -> NodeMetadata {
        NodeMetadata { name: "track" }
    }

    fn add_visited_by(&mut self, visitor_id: String) {
        self.base.visitation_map.insert(visitor_id);
    }

    fn has_been_visited_by(&self, visitor_id: String) -> bool {
        self.base.visitation_map.contains(&visitor_id)
    }
}

impl TypeMapKey for TrackNode {
    type Value = NodeRegistryValue;
}
