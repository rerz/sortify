use crate::graph::edge::Edge;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct OnAlbumEdge;

#[typetag::serde]
impl Edge for OnAlbumEdge {
    fn box_clone(&self) -> Box<dyn Edge> {
        Box::new(self.clone())
    }

    fn get_type(&self) -> &'static str {
        "on_album"
    }
}
