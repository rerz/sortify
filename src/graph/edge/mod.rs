use downcast_rs::{impl_downcast, DowncastSync};
use std::fmt::Formatter;

pub mod on_album;
pub mod on_playlist;
pub mod on_track;
pub mod related_to;

#[typetag::serde(tag = "type")]
pub trait Edge: Send + Sync + DowncastSync {
    fn box_clone(&self) -> Box<dyn Edge>;

    fn get_type(&self) -> &'static str;
}

impl_downcast!(sync Edge);

impl std::fmt::Debug for dyn Edge {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Edge")
    }
}

impl std::fmt::Display for dyn Edge {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Clone for Box<dyn Edge> {
    fn clone(&self) -> Self {
        self.box_clone()
    }
}
