use crate::graph::edge::Edge;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RelatedToEdge;

#[typetag::serde]
impl Edge for RelatedToEdge {
    fn box_clone(&self) -> Box<dyn Edge> {
        Box::new(self.clone())
    }

    fn get_type(&self) -> &'static str {
        "related"
    }
}
