Add a .env file in the project root.

```
SPOTIFY_ID=<your spotify client ID>
SPOTIFY_SECRET=<your spotify client secret>
```

Run backend: `cargo run`, backend runs on localhost:3030
Run frontend: `cd sortify && yarn start`, frontend runs on localhost:4200